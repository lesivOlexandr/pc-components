# PC-component-for-mr.Poltoratsky
## Technology stack
 - [Node.js](https://nodejs.org/en/) - Tool for running server-side js
 - [Fastify](https://www.fastify.io/) - Highly productive Node.js framework for serving requests
 - [Swagger](https://swagger.io/specification/v2/) - Specification for API documentation generation
 - [Typescript](https://www.fastify.io/) - Brings static typification to js
 - [Vue.js](https://vuejs.org/) / [Vue Router](https://router.vuejs.org/) / [Vuex](https://vuex.vuejs.org/) - frontend framework for building reactive interfaces
 - [Webpack](https://webpack.js.org/) - ES5 modules bundler