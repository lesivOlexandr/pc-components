export const getEnvironmentVariable = (variableName: string, defaultValue: string | null = null): string | null => {
  const envVariableValue = process.env[variableName];
  if (envVariableValue === undefined) {
    console.warn(`Environment variable ${variableName} is not set`);
    return defaultValue;
  }
  return envVariableValue;
}