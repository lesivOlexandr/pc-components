import { getEnvironmentVariable } from '@/helpers';
import axios, { AxiosInstance, AxiosResponse, AxiosError } from 'axios';

const BASE_URL = getEnvironmentVariable('VUE_APP_SERVER_URL', 'http://localhost:5001') + '/api';
console.log(BASE_URL);

class Api {
  baseUrl: string;
  instance: AxiosInstance;
  constructor() {
    this.baseUrl = BASE_URL;
    this.instance = axios.create({
      baseURL: BASE_URL,
      headers: {
        'Content-Type': 'application/json',
      },
    });
  }

  async get(url: string, params?: unknown) {
    return await this.instance
      .get(url)
      .then(this.handleResponse)
      .catch(this.handleError);
  }

  async post(url: string, data: unknown) {
    return await this.instance
      .post(url, data)
      .then(this.handleResponse)
      .catch(this.handleError);
  }

  async put(url: string, data: unknown) {
    return await this.instance
      .put(url, data)
      .then(this.handleResponse)
      .catch(this.handleError);
  }

  async delete(url: string, data?: unknown) {
    return await this.instance
      .delete(url)
      .then(this.handleResponse)
      .catch(this.handleError);
  }

  private handleResponse(response: AxiosResponse) {
    return response.data;
  }

  private handleError(error: AxiosError) {
    if (error.response) {
      throw new Error(error.response.data.error);
    } else if (error.request) {
      throw new Error(error.request.responseText);
    } else {
      throw new Error(error.message);
    }
  }
}

export default new Api();