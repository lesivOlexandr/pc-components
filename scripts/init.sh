script_path=$(dirname "$(readlink -f "$0")")
database_name=$1
username=$2

function install_postgress {
  echo "Installing postgres"
  echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list
  wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
  apt-get update
  apt-get -y install postgresql
  echo Postgress installed
}

function install_node {
  curl -sL https://deb.nodesource.com/setup_15.x | sudo -E bash -
  sudo apt-get install -y nodejs
}

function install_backend {
  echo "Installing backend"
  cd $script_path/../server
  npm install
  echo "Backed Installed"
  echo "Executing migrations and seeders"
  npm run db:init
  echo "Migrations and seeders done"
}

install_postgress
install_backend
