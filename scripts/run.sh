script_path=$(dirname "$(readlink -f "$0")")

function run_backend {
  cd $script_path/../server
  npm run build
  npm run start
}

run_backend
