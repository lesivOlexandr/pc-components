import { Sequelize } from 'sequelize';
import * as config from '../../../config/db.config';
import fp from 'fastify-plugin';

import { CpuStatic } from '../models/cpu';
import { GpuStatic } from '../models/gpu';
import { MotherboardStatic } from '../models/motherboard';
import { PowerSupplyStatic } from '../models/powersupply';
import { RamStatic } from '../models/ram';
import { RamTypeStatic } from '../models/ramtype';
import { SetupStatic } from '../models/setup';
import { SocketStatic } from '../models/socket';
import { initializeModels } from '../models/index';
import { initializeRepositories, Repositories } from '../repositories';
import { HddStatic } from '../models/hdd';
import { SsdStatic } from '../models/ssd';

export interface Models {
  Cpu: CpuStatic;
  Gpu: GpuStatic;
  Motherboard: MotherboardStatic;
  PowerSupply: PowerSupplyStatic;
  Ram: RamStatic;
  RamType: RamTypeStatic;
  Setup: SetupStatic;
  Socket: SocketStatic;
  Hdd: HddStatic;
  Ssd: SsdStatic;
}

export interface Db {
  models: Models;
}

export default fp(async (fastify, opts, next) => {
  const { database, username, password, ...params } = config;
  const sequelize = process.env.DATABASE_URL
    ? new Sequelize(process.env.DATABASE_URL, params)
    : new Sequelize(database, username, password, params);

  try {
    await sequelize.authenticate();
    console.log('Connection has been established successfully.');

    const models: Models = initializeModels(sequelize);
    const repositories: Repositories = initializeRepositories(models);

    fastify.decorate('db', { models });
    fastify.decorate('repositories', repositories);
    console.log('repositories were successfully initialized');
    next();
  } catch (err) {
    console.error('Unable to connect to the database:', err);
    next(err);
  }
});
