import { Sequelize } from 'sequelize/types';
import { CpuFactory } from './cpu';
import { GpuFactory } from './gpu';
import { MotherboardFactory } from './motherboard';
import { PowerSupplyFactory } from './powersupply';
import { RamFactory } from './ram';
import { RamTypeFactory } from './ramtype';
import { SetupFactory } from './setup';
import { SocketFactory } from './socket';
import { HddFactory } from './hdd';
import { SsdFactory } from './ssd';

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export const initializeModels = (orm: Sequelize) => {
  const CpuModel = CpuFactory(orm);
  const GpuModel = GpuFactory(orm);
  const MotherboardModel = MotherboardFactory(orm);
  const PowerSupplyModel = PowerSupplyFactory(orm);
  const RamModel = RamFactory(orm);
  const RamTypeModel = RamTypeFactory(orm);
  const SetupModel = SetupFactory(orm);
  const SocketModel = SocketFactory(orm);
  const HddModel = HddFactory(orm);
  const SsdModel = SsdFactory(orm);

  CpuModel.belongsTo(SocketModel);
  CpuModel.hasMany(SetupModel);

  GpuModel.hasMany(SetupModel);

  MotherboardModel.belongsTo(SocketModel);
  MotherboardModel.belongsTo(RamTypeModel);

  PowerSupplyModel.hasMany(SetupModel);

  HddModel.hasMany(SetupModel);

  SsdModel.hasMany(SetupModel);

  RamModel.belongsTo(RamTypeModel, { foreignKey: 'typeId' });
  RamModel.hasMany(SetupModel);

  RamTypeModel.hasMany(RamModel, { foreignKey: 'typeId' });
  RamTypeModel.hasMany(MotherboardModel);

  SetupModel.belongsTo(CpuModel);
  SetupModel.belongsTo(GpuModel);
  SetupModel.belongsTo(RamModel);
  SetupModel.belongsTo(MotherboardModel);
  SetupModel.belongsTo(PowerSupplyModel);
  SetupModel.belongsTo(HddModel);
  SetupModel.belongsTo(SsdModel);
  SetupModel.belongsTo(SetupModel, { foreignKey: 'parentId' });

  CpuModel.belongsTo(SocketModel, { foreignKey: 'socketId' });
  CpuModel.hasMany(SetupModel);

  SocketModel.hasMany(CpuModel, { foreignKey: 'socketId' });
  SocketModel.hasMany(MotherboardModel);

  // eslint-disable-next-line prettier/prettier
  return {
    Cpu: CpuModel,
    Gpu: GpuModel,
    Motherboard: MotherboardModel,
    PowerSupply: PowerSupplyModel,
    Ram: RamModel,
    RamType: RamTypeModel,
    Setup: SetupModel,
    Socket: SocketModel,
    Hdd: HddModel,
    Ssd: SsdModel,
  };
};
