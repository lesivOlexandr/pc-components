import { SetupCreationAttributes, SetupModel, SetupStatic } from '../models/setup';
import { BaseRepository, RichModel, IWithMeta } from './base.repository';
import { IFilter } from './filters/base.filter';
import { CpuStatic } from '../models/cpu';
import { SocketStatic } from '../models/socket';
import { GpuStatic } from '../models/gpu';
import { MotherboardStatic } from '../models/motherboard';
import { RamStatic } from '../models/ram';
import { RamTypeStatic } from '../models/ramtype';
import { PowerSupplyStatic } from '../models/powersupply';
import { ISetupFilter } from './filters/setup.filer';
import { mergeFilters } from './filters/helper';
import { HddStatic } from '../models/hdd';
import { SsdStatic } from '../models/ssd';
import { Op, OrderItem } from 'sequelize';
import { triggerServerError } from '../../helpers/global.helper';

export class SetupRepository extends BaseRepository<SetupModel, SetupCreationAttributes> {
  constructor(
    private model: SetupStatic,
    private cpuModel: CpuStatic,
    private gpuModel: GpuStatic,
    private motherBoardModel: MotherboardStatic,
    private ramModel: RamStatic,
    private powerSupplyModel: PowerSupplyStatic,
    private hddModel: HddStatic,
    private ssdModel: SsdStatic,
    private socketModel: SocketStatic,
    private ramTypeModel: RamTypeStatic
  ) {
    super(<RichModel>model, IFilter);
  }

  getOrderProperty(orderBy: string): OrderItem {
    switch (orderBy) {
      case 'newest':
        return ['createdAt', 'DESC'];
      case 'oldest':
        return ['createdAt', 'ASC'];
      default:
        triggerServerError('You have not supplied valid value for getOrderPropertyFunc', 500);
    }
  }

  async getSetups(inputFilter: ISetupFilter): Promise<IWithMeta<SetupModel>> {
    const filter = mergeFilters<ISetupFilter>(new ISetupFilter(), inputFilter);
    const where: { authorId?: string } = {};
    const result = await this.getAll({
      group: ['setup.id', 'cpu.id', 'gpu.id', 'ram.id', 'powerSupply.id', 'motherboard.id', 'hdd.id', 'ssd.id'],
      order: [this.getOrderProperty(filter.sort)],
      include: [
        {
          model: this.cpuModel,
          as: 'cpu',
        },
        {
          model: this.gpuModel,
        },
        {
          model: this.motherBoardModel,
        },
        {
          model: this.ramModel,
        },
        {
          model: this.powerSupplyModel,
        },
        {
          model: this.hddModel,
        },
        {
          model: this.ssdModel,
        },
      ],
      where: {
        ...where,
        id: { [Op.and]: { [Op.or]: filter.id } },
      },
      subQuery: false,
      offset: filter.from,
      limit: filter.count,
    });

    return result;
  }

  async getAllSetupsBasic(inputFilter: ISetupFilter): Promise<IWithMeta<SetupModel>> {
    const filter = mergeFilters<ISetupFilter>(new ISetupFilter(), inputFilter);
    const where: { authorId?: string } = {};
    const result = await this.getAll({
      group: ['setup.id', 'cpu.id', 'gpu.id', 'ram.id', 'powerSupply.id', 'motherboard.id', 'hdd.id', 'ssd.id'],
      order: [this.getOrderProperty('newest')],
      include: [
        {
          model: this.cpuModel,
          as: 'cpu',
        },
        {
          model: this.gpuModel,
        },
        {
          model: this.motherBoardModel,
        },
        {
          model: this.ramModel,
        },
        {
          model: this.powerSupplyModel,
        },
        {
          model: this.hddModel,
        },
        {
          model: this.ssdModel,
        },
      ],
      where,
      subQuery: false,
      offset: filter.from,
      limit: filter.count,
    });

    return result;
  }

  async getOneSetup(id: string): Promise<SetupModel> {
    const setup = await this.model.findByPk(id, {
      group: [
        'setup.id',
        'cpu.id',
        'gpu.id',
        'ram.id',
        'powerSupply.id',
        'motherboard.id',
        'hdd.id',
        'ssd.id',
        'author.id',
        'cpu->socket.id',
        'motherboard->socket.id',
        'motherboard->ramType.id',
        'ram->ramType.id',
      ],
      include: [
        {
          model: this.cpuModel,
          include: [
            {
              model: this.socketModel,
            },
          ],
          as: 'cpu',
        },
        {
          model: this.gpuModel,
          as: 'gpu',
        },
        {
          model: this.ramModel,
          include: [
            {
              model: this.ramTypeModel,
            },
          ],
          as: 'ram',
        },
        {
          model: this.powerSupplyModel,
          as: 'powerSupply',
        },
        {
          model: this.motherBoardModel,
          include: [
            {
              model: this.socketModel,
            },
            {
              model: this.ramTypeModel,
            },
          ],
          as: 'motherboard',
        },
        {
          model: this.hddModel,
          as: 'hdd',
        },
        {
          model: this.ssdModel,
          as: 'ssd',
        },
      ],
    });
    return setup;
  }
}
