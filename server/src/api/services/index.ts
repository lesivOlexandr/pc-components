import fp from 'fastify-plugin';

import { CpuService } from './cpu.service';
import { GpuService } from './gpu.service';
import { MotherboardService } from './motherboard.service';
import { PowerSupplyService } from './powerSupply.service';
import { RamService } from './ram.service';
import { RamTypeService } from './ramType.service';
import { SocketService } from './socket.service';
import { SetupService } from './setup.service';
import { HddService } from './hdd.service';
import { SsdService } from './ssd.service';

export interface Services {
  CpuService: CpuService;
  HddService: HddService;
  GpuService: GpuService;
  MotherboardService: MotherboardService;
  PowerSupplyService: PowerSupplyService;
  RamService: RamService;
  RamTypeService: RamTypeService;
  SetupService: SetupService;
  SocketService: SocketService;
  SsdService: SsdService;
}

export default fp(async (fastify, opts, next) => {
  try {
    const { repositories } = fastify;
    const ramTypeService = new RamTypeService(repositories.RamTypeRepository);
    const setupService = new SetupService(repositories.SetupRepository);
    const ramService = new RamService(repositories.RamRepository);
    const powerSupplyService = new PowerSupplyService(repositories.PowerSupplyRepository);
    const socketService = new SocketService(repositories.SocketRepository);
    const motherboardService = new MotherboardService(repositories.MotherboardRepository);
    const gpuService = new GpuService(repositories.GpuRepository);
    const cpuService = new CpuService(repositories.CpuRepository);

    const hddService = new HddService(repositories.HddRepository);
    const ssdService = new SsdService(repositories.SsdRepository);
    const services: Services = {
      CpuService: cpuService,
      HddService: hddService,
      GpuService: gpuService,
      MotherboardService: motherboardService,
      PowerSupplyService: powerSupplyService,
      RamService: ramService,
      RamTypeService: ramTypeService,
      SetupService: setupService,
      SocketService: socketService,
      SsdService: ssdService,
    };
    fastify.decorate('services', services);
    console.log('services were successfully initialized');
    next();
  } catch (err) {
    console.error('Unable to initialize services:', err);
    next(err);
  }
});
