import { FastifyInstance } from 'fastify';
import { FastifyDone, FastifyOptions } from './fastifyTypes';
import {
  GetSetupsRequest,
  GetSetupRequest,
  PostSetupRequest,
  DeleteSetupRequest,
  PutSetupRequest,
  CreateSetupSchema,
  UpdateSetupSchema,
  GetAllSetupsResponse,
  DetailedSetupSchema,
} from './setup.schema';

import { SetupMiddleware } from '../middlewares/setup.middleware';
import {
  createOneQuery,
  updateOneQuery,
  getOneQuery,
  getMultipleQuery,
  deleteOneQuery,
} from '../../helpers/swagger.helper';

export function router(fastify: FastifyInstance, opts: FastifyOptions, next: FastifyDone): void {
  const { SetupService } = fastify.services;

  const setupMiddleware = SetupMiddleware(fastify);

  const getAllSchema = getMultipleQuery(GetAllSetupsResponse);
  fastify.get('/', getAllSchema, async (request: GetSetupsRequest, reply) => {
    const setups = await SetupService.getAllSetups(request.query);
    reply.send(setups);
  });

  const getOneSchema = getOneQuery(DetailedSetupSchema, undefined);
  fastify.get('/:id', getOneSchema, async function (request: GetSetupRequest, reply) {
    const { id } = request.params;
    const setup = await SetupService.getSetupById(id);
    reply.send(setup);
  });

  const createOneSchema = createOneQuery(CreateSetupSchema, DetailedSetupSchema);
  fastify.post('/', createOneSchema, async (request: PostSetupRequest, reply) => {
    const data = { ...request.body };
    const setup = await SetupService.createSetup(data, setupMiddleware);
    reply.send(setup);
  });

  const updateOneSchema = updateOneQuery(UpdateSetupSchema, DetailedSetupSchema);
  fastify.put('/:id', updateOneSchema, async (request: PutSetupRequest, reply) => {
    const { id } = request.params;
    const setup = await SetupService.updateSetupById({ id, data: request.body }, setupMiddleware);
    reply.send(setup);
  });

  const deleteOneSchema = deleteOneQuery(DetailedSetupSchema);
  fastify.delete('/:id', deleteOneSchema, async (request: DeleteSetupRequest, reply) => {
    const { id } = request.params;
    const setup = await SetupService.deleteSetupById(id);
    reply.send(setup);
  });

  next();
}
