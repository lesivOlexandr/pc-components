import { SocketSchema } from './socket.schema';
import { RamTypeSchema } from './ramType.schema';
import { RamSchema } from './ram.schema';
import { PowerSupplySchema } from './powerSupply.schema';
import { MotherBoardSchema } from './motherboard.schema';
import { GpuSchema } from './gpu.schema';
import { CpuSchema } from './cpu.schema';
import { FastifyRegisterOptions } from 'fastify';
import { SwaggerOptions } from 'fastify-swagger';
import { SsdSchema } from './ssd.schema';
import { HddSchema } from './hdd.schema';

const SwaggerMainSchema: FastifyRegisterOptions<SwaggerOptions> = {
  swagger: {
    info: {
      title: 'pc-components-for-mr-Poltoratsky',
      version: '1.0.0',
    },
    definitions: {
      Socket: SocketSchema,
      RamType: RamTypeSchema,
      Ram: RamSchema,
      PowerSupply: PowerSupplySchema,
      MotherBoard: MotherBoardSchema,
      Gpu: GpuSchema,
      Cpu: CpuSchema,
      Ssd: SsdSchema,
      Hdd: HddSchema,
    },
  },
  exposeRoute: true,
  routePrefix: '/documentation',
};

export default SwaggerMainSchema;
