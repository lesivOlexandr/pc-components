import { SwaggerSchema } from '../data/models/swaggerSchema';
import { RouteShorthandOptions } from 'fastify/types/route';

export function getOneQuery(
  schema: SwaggerSchema,
  querystring?: SwaggerSchema,
  isProtected = true
): RouteShorthandOptions {
  return {
    schema: {
      params: {
        id: { type: 'integer', nullable: false, minimum: 1 },
      },
      querystring,
      response: {
        200: schema,
        404: getNotFoundHeader(),
        ...(isProtected && { 403: getForbiddenHeader() }),
      },
    },
  };
}

export function createOneQuery(
  request: SwaggerSchema,
  response: SwaggerSchema,
  isProtected = true
): RouteShorthandOptions {
  return {
    schema: {
      body: { ...request, additionalProperties: false },
      response: {
        200: response,
        ...(isProtected && { 403: getForbiddenHeader() }),
      },
    },
  };
}

export function updateOneQuery(
  toUpdate: SwaggerSchema,
  newData: SwaggerSchema,
  isProtected = true
): RouteShorthandOptions {
  return {
    schema: {
      params: {
        id: {
          type: 'integer',
          nullable: false,
          minimum: 1,
        },
      },
      body: { ...toUpdate, additionalProperties: false, minProperties: 1 },
      response: {
        200: newData,
        404: getNotFoundHeader(),
        ...(isProtected && { 403: getForbiddenHeader() }),
      },
    },
  };
}

export function deleteOneQuery(schema?: SwaggerSchema, isProtected = true): RouteShorthandOptions {
  return {
    schema: {
      params: {
        id: {
          type: 'integer',
          nullable: false,
          minimum: 1,
        },
      },
      response: {
        ...(!!schema && { 200: schema }),
        404: getNotFoundHeader(),
        ...(isProtected && { 403: getForbiddenHeader() }),
      },
    },
  };
}

export function getMultipleQuery(
  schema: SwaggerSchema,
  querystring?: SwaggerSchema,
  isProtected = true
): RouteShorthandOptions {
  return {
    schema: {
      querystring,
      response: {
        200: schema,
        ...(isProtected && { 403: getForbiddenHeader() }),
      },
    },
  };
}

function getForbiddenHeader() {
  return {
    type: 'object',
    properties: {
      error: {
        type: 'string',
        minLength: 1,
        example: 'Access Denied',
        nullable: false,
      },
      status: {
        type: 'integer',
        nullable: false,
        example: 403,
      },
    },
    nullable: false,
  };
}

function getNotFoundHeader() {
  return {
    type: 'object',
    properties: {
      error: {
        type: 'string',
        minLength: 1,
        example: 'Item with id: 1 does not exists',
        nullable: false,
      },
      status: {
        type: 'integer',
        nullable: false,
        example: 404,
      },
    },
    nullable: false,
  };
}
